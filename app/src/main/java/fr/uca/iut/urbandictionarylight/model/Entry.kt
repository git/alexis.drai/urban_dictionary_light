package fr.uca.iut.urbandictionarylight.model

import androidx.room.Entity
import androidx.room.PrimaryKey

const val NEW_ENTRY_ID = 0L

@Entity(tableName = "entries")
data class Entry(
    val phrase: String = "",
    @PrimaryKey(autoGenerate = true) val entryId: Long = NEW_ENTRY_ID
)