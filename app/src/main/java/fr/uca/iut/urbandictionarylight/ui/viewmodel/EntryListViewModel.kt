package fr.uca.iut.urbandictionarylight.ui.viewmodel

import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.uca.iut.urbandictionarylight.data.UrbanDictionaryDatabase
import fr.uca.iut.urbandictionarylight.data.entry.EntryRepository
import fr.uca.iut.urbandictionarylight.model.EntryWithDefinitions
import kotlinx.coroutines.launch

class EntryListViewModel : ViewModel() {
    private val entryRepo = EntryRepository(UrbanDictionaryDatabase.getInstance().entryDao())

    val entryList = entryRepo.getAll()

    fun delete(entry: EntryWithDefinitions) = viewModelScope.launch { entryRepo.delete(entry) }

}
