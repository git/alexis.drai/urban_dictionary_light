package fr.uca.iut.urbandictionarylight.ui.activity

import android.os.Bundle
import android.widget.FrameLayout
import fr.uca.iut.urbandictionarylight.R
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID
import fr.uca.iut.urbandictionarylight.ui.fragment.EntryFragment
import fr.uca.iut.urbandictionarylight.ui.fragment.EntryListFragment

class EntryListActivity : SimpleFragmentActivity(),
    EntryListFragment.OnInteractionListener, EntryFragment.OnInteractionListener {

    private var isTwoPane: Boolean = false
    private lateinit var masterFragment: EntryListFragment

    override fun createFragment() = EntryListFragment().also { masterFragment = it }
    override fun getLayoutResId() = R.layout.toolbar_md_activity


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        isTwoPane = findViewById<FrameLayout>(R.id.container_fragment_detail) != null
        if (savedInstanceState != null)
            masterFragment = supportFragmentManager.findFragmentById(R.id.container_fragment) as EntryListFragment

        if (!isTwoPane) {
            removeDisplayedFragment()
        }
    }


    override fun onEntrySelected(entryId: Long) {
        if (isTwoPane) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_fragment_detail, EntryFragment.newInstance(entryId))
                .commit()
        } else {
            startActivity(EntryPagerActivity.getIntent(this, entryId))
        }
    }


    override fun onAddNewEntry() = startActivity(EntryActivity.getIntent(this, NEW_ENTRY_ID))


    override fun onEntrySaved() { }


    private fun removeDisplayedFragment() {
        supportFragmentManager.findFragmentById(R.id.container_fragment_detail)?.let {
            supportFragmentManager.beginTransaction().remove(it).commit()
        }
    }


    override fun onEntryDeleted() {
        if (isTwoPane) {
            removeDisplayedFragment()
        } else
            finish()
    }


    override fun onEntrySwiped() {
        if (isTwoPane) {
            removeDisplayedFragment()
        }
    }
}
