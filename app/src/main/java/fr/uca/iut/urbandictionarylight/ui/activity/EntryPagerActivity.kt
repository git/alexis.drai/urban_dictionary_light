package fr.uca.iut.urbandictionarylight.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.LinearLayout
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import fr.uca.iut.urbandictionarylight.R
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID
import fr.uca.iut.urbandictionarylight.ui.fragment.EntryFragment
import fr.uca.iut.urbandictionarylight.ui.utils.EntryPagerAdapter
import fr.uca.iut.urbandictionarylight.ui.viewmodel.EntryPagerViewModel


class EntryPagerActivity : AppCompatActivity(), EntryFragment.OnInteractionListener {

    companion object {
        private const val EXTRA_ENTRY_ID =
            "fr.uca.iut.urbandictionarylight.ui.activity.extra_entry_id"

        fun getIntent(context: Context, entryId: Long) =
            Intent(context, EntryPagerActivity::class.java).apply {
                putExtra(EXTRA_ENTRY_ID, entryId)
            }
    }

    private val pagerAdapter = EntryPagerAdapter(this)
    private val entryPagerVM by viewModels<EntryPagerViewModel>()
    private lateinit var viewPager: ViewPager2

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pager)

        setSupportActionBar(findViewById(R.id.toolbar_activity))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        viewPager = ViewPager2(this)
        viewPager.id = R.id.view_pager
        findViewById<LinearLayout>(R.id.pager_layout).addView(viewPager)

        viewPager.adapter = pagerAdapter
        entryPagerVM.currentEntryId =
            savedInstanceState?.getLong(EXTRA_ENTRY_ID) ?: intent.getLongExtra(
                EXTRA_ENTRY_ID,
                NEW_ENTRY_ID
            )

        entryPagerVM.entryList.observe(this) {
            pagerAdapter.submitList(it)
            var position = pagerAdapter.positionFromId(entryPagerVM.currentEntryId)
            if (position == -1) position = 0
            viewPager.currentItem = position
        }

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                entryPagerVM.currentEntryId = pagerAdapter.entryIdAt(position)
            }
        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(EXTRA_ENTRY_ID, entryPagerVM.currentEntryId)
    }

    override fun onEntrySaved() = finish()
    override fun onEntryDeleted() = finish()
}

