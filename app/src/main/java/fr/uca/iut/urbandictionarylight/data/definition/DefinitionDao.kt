package fr.uca.iut.urbandictionarylight.data.definition

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.uca.iut.urbandictionarylight.model.Definition

@Dao
interface DefinitionDao {
    @Query("SELECT * FROM definitions")
    fun queryAll(): LiveData<List<Definition>>

    @Query("SELECT * FROM entries WHERE definitionId = :id")
    fun query(id: Long): LiveData<Definition>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: Definition)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(entry: Definition)

    @Delete
    suspend fun delete(entry: Definition)
}