package fr.uca.iut.urbandictionarylight.ui.utils

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.uca.iut.urbandictionarylight.databinding.ItemListEntryBinding
import fr.uca.iut.urbandictionarylight.model.EntryWithDefinitions

class EntryRecyclerViewAdapter(private val listener: Callbacks) :
    ListAdapter<EntryWithDefinitions, EntryRecyclerViewAdapter.EntryViewHolder>(
        DiffUtilEntryCallback
    ) {

    private object DiffUtilEntryCallback : DiffUtil.ItemCallback<EntryWithDefinitions>() {
        override fun areItemsTheSame(oldItem: EntryWithDefinitions, newItem: EntryWithDefinitions) =
            oldItem.entry.entryId == newItem.entry.entryId

        override fun areContentsTheSame(
            oldItem: EntryWithDefinitions,
            newItem: EntryWithDefinitions
        ) = oldItem == newItem
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        EntryViewHolder(
            ItemListEntryBinding.inflate(LayoutInflater.from(parent.context)), listener
        )


    override fun onBindViewHolder(holder: EntryViewHolder, position: Int) =
        holder.bind(getItem(position))


    class EntryViewHolder(private val binding: ItemListEntryBinding, listener: Callbacks) :
        RecyclerView.ViewHolder(binding.root) {

        val entry: EntryWithDefinitions? get() = binding.entryWithDefinitions

        init {
            itemView.setOnClickListener { entry?.let { listener.onEntrySelected(it.entry.entryId) } }
        }

        fun bind(entryWithDefinitions: EntryWithDefinitions) {
            binding.entryWithDefinitions = entryWithDefinitions
            binding.executePendingBindings()
        }

    }

    interface Callbacks {
        fun onEntrySelected(entryId: Long)
    }
}
