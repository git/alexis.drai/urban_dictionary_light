package fr.uca.iut.urbandictionarylight.data.entry

import androidx.lifecycle.LiveData
import androidx.room.*
import fr.uca.iut.urbandictionarylight.model.Definition
import fr.uca.iut.urbandictionarylight.model.Entry
import fr.uca.iut.urbandictionarylight.model.EntryWithDefinitions


@Dao
interface EntryDao {
    @Transaction
    @Query("SELECT * FROM entries")
    fun queryAll(): LiveData<List<EntryWithDefinitions>>

    @Transaction
    @Query("SELECT * FROM entries WHERE entryId = :id")
    fun query(id: Long): LiveData<EntryWithDefinitions>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: EntryWithDefinitions) {
        val entryId: Long = insert(entry.entry)
        for (def in entry.definitions) {
            def.correspondingEntryId = entryId
            insert(def)
        }
    }

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: Entry): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entry: Definition)

    @Update
    suspend fun update(entry: EntryWithDefinitions)

    @Delete
    suspend fun delete(entry: EntryWithDefinitions)
}