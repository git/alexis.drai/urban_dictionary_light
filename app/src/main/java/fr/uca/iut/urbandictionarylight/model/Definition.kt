package fr.uca.iut.urbandictionarylight.model

import androidx.room.Entity
import androidx.room.PrimaryKey

const val NEW_DEFINITION_ID = 0L

@Entity(tableName = "definitions")
data class Definition(
    val content: String,
    val example: String,
    var upvotes: Int = 0,
    var correspondingEntryId: Long,
    @PrimaryKey(autoGenerate = true) val definitionId: Long = NEW_DEFINITION_ID
)