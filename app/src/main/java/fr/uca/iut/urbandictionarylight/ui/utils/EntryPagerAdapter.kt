package fr.uca.iut.urbandictionarylight.ui.utils

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import fr.uca.iut.urbandictionarylight.model.EntryWithDefinitions
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID
import fr.uca.iut.urbandictionarylight.ui.fragment.EntryFragment

class EntryPagerAdapter(fragmentActivity: FragmentActivity) :
    FragmentStateAdapter(fragmentActivity) {
    private var entryList = listOf<EntryWithDefinitions>()

    override fun getItemCount() = entryList.size

    override fun createFragment(position: Int) =
        EntryFragment.newInstance(entryList[position].entry.entryId)

    fun positionFromId(entryId: Long) = entryList.indexOfFirst { it.entry.entryId == entryId }

    fun entryIdAt(position: Int) =
        if (entryList.isEmpty()) NEW_ENTRY_ID else entryList[position].entry.entryId

    fun submitList(entryList: List<EntryWithDefinitions>) {
        this.entryList = entryList
        notifyDataSetChanged()
    }
}