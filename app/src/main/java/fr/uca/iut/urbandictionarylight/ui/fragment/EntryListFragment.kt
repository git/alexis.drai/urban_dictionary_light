package fr.uca.iut.urbandictionarylight.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import fr.uca.iut.urbandictionarylight.R
import fr.uca.iut.urbandictionarylight.databinding.FragmentListEntryBinding
import fr.uca.iut.urbandictionarylight.ui.utils.EntryRecyclerViewAdapter
import fr.uca.iut.urbandictionarylight.ui.viewmodel.EntryListViewModel


class EntryListFragment : Fragment(), EntryRecyclerViewAdapter.Callbacks {

    private val entryListAdapter = EntryRecyclerViewAdapter(this)
    private val entryListVM by viewModels<EntryListViewModel>()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentListEntryBinding.inflate(inflater)
        binding.entryListVM = entryListVM
        binding.lifecycleOwner = viewLifecycleOwner
        with(binding.entriesRecyclerView) {
            adapter = entryListAdapter
            ItemTouchHelper(EntryListItemTouchHelper()).attachToRecyclerView(this)
        }
        binding.addEntryBtn.setOnClickListener { addNewEntry() }
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMenu()

        entryListVM.entryList.observe(viewLifecycleOwner) {
            entryListAdapter.submitList(it)
        }
    }


    private fun setupMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.fragment_list_entry, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.menu_item_new_entry -> {
                        addNewEntry()
                        true
                    }
                    else -> false
                }
            }
        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }


    private fun addNewEntry() {
        listener?.onAddNewEntry()
    }


    override fun onEntrySelected(entryId: Long) {
        listener?.onEntrySelected(entryId)
    }


    private inner class EntryListItemTouchHelper : ItemTouchHelper.Callback() {
        override fun isLongPressDragEnabled() = false

        override fun getMovementFlags(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder
        ) =
            makeMovementFlags(
                ItemTouchHelper.UP or ItemTouchHelper.DOWN,
                ItemTouchHelper.START or ItemTouchHelper.END
            )

        override fun onMove(
            recyclerView: RecyclerView,
            viewHolder: RecyclerView.ViewHolder,
            target: RecyclerView.ViewHolder
        ) = false

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            (viewHolder as EntryRecyclerViewAdapter.EntryViewHolder).entry?.also {
                entryListVM.delete(it)
                listener?.onEntrySwiped()
            }
        }
    }


    interface OnInteractionListener {
        fun onEntrySelected(entryId: Long)
        fun onAddNewEntry()
        fun onEntrySwiped()
    }

    private var listener: OnInteractionListener? = null


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnInteractionListener")
        }
    }


    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
