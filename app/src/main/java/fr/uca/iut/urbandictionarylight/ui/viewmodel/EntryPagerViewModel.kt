package fr.uca.iut.urbandictionarylight.ui.viewmodel

import androidx.lifecycle.ViewModel
import fr.uca.iut.urbandictionarylight.data.UrbanDictionaryDatabase
import fr.uca.iut.urbandictionarylight.data.entry.EntryRepository
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID

class EntryPagerViewModel : ViewModel() {
    private val entryRepo = EntryRepository(UrbanDictionaryDatabase.getInstance().entryDao())
    val entryList = entryRepo.getAll()
    var currentEntryId = NEW_ENTRY_ID
}