package fr.uca.iut.urbandictionarylight.data

import android.app.Application
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import fr.uca.iut.urbandictionarylight.data.definition.DefinitionDao
import fr.uca.iut.urbandictionarylight.data.entry.EntryDao
import fr.uca.iut.urbandictionarylight.model.Definition
import fr.uca.iut.urbandictionarylight.model.Entry

private const val DB_FILE = "u_d_light.db"

@Database(entities = [Entry::class, Definition::class], version = 1)
abstract class UrbanDictionaryDatabase : RoomDatabase() {

    abstract fun entryDao(): EntryDao
    abstract fun definitionDao(): DefinitionDao

    companion object {
        private lateinit var application: Application

        @Volatile
        private var instance: UrbanDictionaryDatabase? = null

        fun getInstance(): UrbanDictionaryDatabase {
            if (Companion::application.isInitialized) {
                if (instance == null)
                    synchronized(this) {
                        if (instance == null) {
                            instance = Room.databaseBuilder(
                                application.applicationContext,
                                UrbanDictionaryDatabase::class.java,
                                DB_FILE
                            )
                                .build()
                        }
                    }
                return instance!!
            } else
                throw RuntimeException("must initialize DB first")
        }

        @Synchronized
        fun initialize(app: Application) {
            if (Companion::application.isInitialized)
                throw RuntimeException("can't initialize DB twice")
            application = app
        }
    }
}
