package fr.uca.iut.urbandictionarylight.ui.fragment

import android.content.Context
import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AlertDialog
import androidx.core.os.bundleOf
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.get
import androidx.lifecycle.viewmodel.viewModelFactory
import fr.uca.iut.urbandictionarylight.R
import fr.uca.iut.urbandictionarylight.databinding.FragmentEntryBinding
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID
import fr.uca.iut.urbandictionarylight.ui.viewmodel.EntryViewModel

class EntryFragment : Fragment() {

    companion object {
        private const val EXTRA_ENTRY_ID =
            "fr.uca.iut.urbandictionarylight.ui.activity.extra_entry_id"

        fun newInstance(entryId: Long) = EntryFragment().apply {
            arguments = bundleOf(EXTRA_ENTRY_ID to entryId)
        }
    }

    private var entryId: Long = NEW_ENTRY_ID
    private lateinit var entryVM: EntryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        entryId = savedInstanceState?.getLong(EXTRA_ENTRY_ID) ?: arguments?.getLong(EXTRA_ENTRY_ID)
                ?: NEW_ENTRY_ID


        if (entryId == NEW_ENTRY_ID) {
            requireActivity().setTitle(R.string.add_entry_lbl)
        }

        entryVM = ViewModelProvider(this, viewModelFactory { EntryViewModel(entryId) }).get()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putLong(EXTRA_ENTRY_ID, entryId)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val binding = FragmentEntryBinding.inflate(inflater)
        binding.entryVM = entryVM
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupMenu()
    }

    private fun setupMenu() {
        requireActivity().addMenuProvider(object : MenuProvider {
            override fun onPrepareMenu(menu: Menu) {
                super.onPrepareMenu(menu)
                if (entryId == NEW_ENTRY_ID) {
                    menu.findItem(R.id.action_delete)?.isVisible = false
                }
            }

            override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
                menuInflater.inflate(R.menu.fragment_entry, menu)
            }

            override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
                return when (menuItem.itemId) {
                    R.id.action_save -> {
                        saveEntry()
                        true
                    }
                    R.id.action_delete -> {
                        deleteEntry()
                        true
                    }
                    else -> false
                }
            }

        }, viewLifecycleOwner, Lifecycle.State.RESUMED)
    }

    private fun saveEntry() {
        if (entryVM.saveEntry() == true) {
            listener?.onEntrySaved()
        } else {
            AlertDialog.Builder(requireActivity())
                .setTitle(R.string.create_entry_error_dialog_title)
                .setMessage(R.string.create_entry_error_message)
                .setNeutralButton(android.R.string.ok, null)
                .show()
            return
        }
    }

    private fun deleteEntry() {
        if (entryId != NEW_ENTRY_ID) {
            entryVM.deleteEntry()
            listener?.onEntryDeleted()
        }
    }

    interface OnInteractionListener {
        fun onEntrySaved()
        fun onEntryDeleted()
    }

    private var listener: OnInteractionListener? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnInteractionListener) {
            listener = context
        } else {
            throw RuntimeException("$context must implement OnInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}
