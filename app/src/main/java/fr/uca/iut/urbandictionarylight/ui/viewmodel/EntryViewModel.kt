package fr.uca.iut.urbandictionarylight.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import fr.uca.iut.urbandictionarylight.data.UrbanDictionaryDatabase
import fr.uca.iut.urbandictionarylight.data.entry.EntryRepository
import fr.uca.iut.urbandictionarylight.model.EntryWithDefinitions
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID
import kotlinx.coroutines.launch


class EntryViewModel(entryId: Long) : ViewModel() {
    private val entryRepo = EntryRepository(UrbanDictionaryDatabase.getInstance().entryDao())

    val entry =
        if (entryId == NEW_ENTRY_ID) MutableLiveData(EntryWithDefinitions()) else entryRepo.findById(
            entryId
        )

    fun saveEntry() = entry.value?.let {
        if (it.entry.phrase.isBlank() || it.definitions.isEmpty())
            false
        else {
            viewModelScope.launch {
                if (it.entry.entryId == NEW_ENTRY_ID) entryRepo.insert(it) else entryRepo.update(it)
            }
            true
        }
    }

    fun deleteEntry() = viewModelScope.launch {
        entry.value?.let { if (it.entry.entryId != NEW_ENTRY_ID) entryRepo.delete(it) }
    }
}