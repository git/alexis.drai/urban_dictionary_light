package fr.uca.iut.urbandictionarylight.data.entry

import android.util.Log
import androidx.lifecycle.LiveData
import fr.uca.iut.urbandictionarylight.model.EntryWithDefinitions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "EntryRepository"

class EntryRepository(private val entryDao: EntryDao) {

    fun getAll(): LiveData<List<EntryWithDefinitions>> {
        Log.i(TAG, "GET ALL ENTRIES")
        return entryDao.queryAll()
    }

    fun findById(entryId: Long): LiveData<EntryWithDefinitions> {
        Log.i(TAG, "GET ENTRY")
        return entryDao.query(entryId)
    }

    suspend fun insert(entry: EntryWithDefinitions) = withContext(Dispatchers.IO) {
        Log.i(TAG, "POST ENTRY")
        entryDao.insert(entry)
    }

    suspend fun delete(entry: EntryWithDefinitions) = withContext(Dispatchers.IO) {
        Log.i(TAG, "DELETE ENTRY")
        entryDao.delete(entry)
    }

    suspend fun update(entry: EntryWithDefinitions) = withContext(Dispatchers.IO) {
        Log.i(TAG, "PUT ENTRY")
        entryDao.update(entry)
    }
}