package fr.uca.iut.urbandictionarylight.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import fr.uca.iut.urbandictionarylight.R
import fr.uca.iut.urbandictionarylight.model.NEW_ENTRY_ID
import fr.uca.iut.urbandictionarylight.ui.fragment.EntryFragment

class EntryActivity : SimpleFragmentActivity(), EntryFragment.OnInteractionListener {
    companion object {
        private const val EXTRA_ENTRY_ID =
            "fr.uca.iut.urbandictionarylight.ui.activity.extra_entry_id"

        fun getIntent(context: Context, entryId: Long) =
            Intent(context, EntryActivity::class.java).apply {
                putExtra(EXTRA_ENTRY_ID, entryId)
            }
    }

    private var entryId = NEW_ENTRY_ID


    override fun onCreate(savedInstanceState: Bundle?) {
        entryId = intent.getLongExtra(EXTRA_ENTRY_ID, NEW_ENTRY_ID)
        super.onCreate(savedInstanceState)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }


    override fun createFragment() = EntryFragment.newInstance(entryId)
    override fun getLayoutResId() = R.layout.toolbar_activity
    override fun onEntrySaved() = finish()
    override fun onEntryDeleted() = finish()
}