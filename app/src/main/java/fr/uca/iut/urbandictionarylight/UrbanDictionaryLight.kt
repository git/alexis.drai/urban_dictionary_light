package fr.uca.iut.urbandictionarylight

import android.app.Application
import fr.uca.iut.urbandictionarylight.data.UrbanDictionaryDatabase

class UrbanDictionaryLight : Application() {
    override fun onCreate() {
        super.onCreate()
        UrbanDictionaryDatabase.initialize(this)
    }
}