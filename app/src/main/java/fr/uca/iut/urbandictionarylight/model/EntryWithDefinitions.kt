package fr.uca.iut.urbandictionarylight.model

import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.Relation

data class EntryWithDefinitions(
    @Embedded val entry: Entry = Entry(),
    @Relation(
        parentColumn = "entryId",
        entityColumn = "correspondingEntryId"
    )
    val definitions: List<Definition> = mutableListOf()
)