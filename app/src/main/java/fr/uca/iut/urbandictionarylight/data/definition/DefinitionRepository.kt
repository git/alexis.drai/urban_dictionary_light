package fr.uca.iut.urbandictionarylight.data.definition

import android.util.Log
import androidx.lifecycle.LiveData
import fr.uca.iut.urbandictionarylight.model.Definition
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private const val TAG = "DefinitionRepository"


class DefinitionRepository(private val definitionDao: DefinitionDao) {

    fun getAll(): LiveData<List<Definition>> {
        Log.i(TAG, "GET ALL DEFINITIONS")
        return definitionDao.queryAll()
    }

    fun findById(definitionId: Long): LiveData<Definition> {
        Log.i(TAG, "GET DEFINITION")
        return definitionDao.query(definitionId)
    }

    suspend fun insert(definition: Definition) = withContext(Dispatchers.IO) {
        Log.i(TAG, "POST DEFINITION")
        definitionDao.insert(definition)
    }

    suspend fun delete(definition: Definition) = withContext(Dispatchers.IO) {
        Log.i(TAG, "DELETE DEFINITION")
        definitionDao.delete(definition)
    }

    suspend fun update(definition: Definition) = withContext(Dispatchers.IO) {
        Log.i(TAG, "PUT DEFINITION")
        definitionDao.update(definition)
    }
}